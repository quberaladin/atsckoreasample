package net.quber.atsckoreasample;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import net.quber.atsckorea.Channel;
import net.quber.atsckorea.ChannelList;
import net.quber.atsckorea.ChannelScanNotify;
import net.quber.atsckorea.Define;
import net.quber.atsckorea.DtvInterface;
import net.quber.atsckorea.OnDtvPlayerListener;

import static net.quber.atsckoreasample.MainActivity.TAG;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ExecutionException;

import static net.quber.atsckorea.Define.ResultEnum.OK;

class QuberTestCentral {
    private static QuberTestCentral instance;
    private static MediaPlayer mPlayer;
    private final Context mContext;
    private DtvInterface mTvInterface;
    private ChannelList mChannelList;
    private ScanObserver mScanObserver;
    private int ChannelPlayIndex = 0;


    class ScanObserver implements ChannelScanNotify {

        @Override
        public void onScanComplete() {
            Log.i(TAG, "Scan Complete!");
        }

        @Override
        public void onScanChannelResult(int major, int minor, String name) {
            Log.i(TAG, "Scanned (" + major + "-" + minor + ") " + name);
        }
    }

    OnDtvPlayerListener onPlaying = new OnDtvPlayerListener(){

        // 채널전환 성공시 호출되는 Method
        @Override
        public void onCompletePlay(int freq, int video_pid, int video_fmt, int audio_pid, int audio_fmt, int pcr_pid) {
            Log.i(TAG, "Playing on Success " + freq);
        }

        // 채널전환 실패시 호출되는 Method
        @Override
        public void onErrorPlay(int e, String msg, int freq, int video_pid, int video_fmt, int audio_pid, int audio_fmt) {
            Log.i(TAG, "Playing on Error " + freq);
        }

        // 현재 채널의 신호 상태를 알려주는 Method
        @Override
        public void onTuneStatus(boolean tunerLocked, int tunerStrength, int tunerQuality) {
        }
    };

    public static QuberTestCentral getInstance(Context ctx) {
        if (instance == null) {
            instance = new QuberTestCentral(ctx);
            instance.mTvInterface = DtvInterface.create(ctx);

            //instance.mTvInterface = new DtvHalDirectInterface(activity);
            mPlayer = new MediaPlayer();
            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.start();
                }
            });
        }

        return instance;
    }
    private QuberTestCentral(Context ctx){
        mContext = ctx;
    }

    public Define.ResultEnum test_VideoPlay_HLS() {
        mTvInterface.dtvStop();
        try {
            try {
                mPlayer.reset();
            } catch (IllegalStateException e) {}
            mPlayer.setDataSource("https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8");
            mPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return OK;
    }

    private void freeMediaPlayer() {
        try {
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
                mPlayer.reset();
            }
        } catch (IllegalStateException e) {}
    }

    private ChannelList getChannelList() throws ExecutionException {
        mChannelList = mTvInterface.loadChannelList(mChannelList);
        return mChannelList;
    }

    public Define.ResultEnum test_ChannelPlayUp() {

        // Media 중지
        freeMediaPlayer();

        // 채널리스트 얻어옴
        ChannelList chlist = null;
        try {
            chlist = getChannelList();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (chlist.size() < 1) {
            Log.e(TAG, "There has no channels");
            return Define.ResultEnum.ERROR;
        }
        if ((ChannelPlayIndex + 1) < chlist.size()) {
            ChannelPlayIndex++;
        } else {
            ChannelPlayIndex = 0;
        }

        // 재생할 채널정보 얻어옴
        Channel ch = chlist.get(ChannelPlayIndex);
        Log.i(TAG, "play " + ch.getServiceName());

        // 채널 재생
        return mTvInterface.dtvPlay(ch.getFreq(), ch.getVideoPid(), ch.getVideoFormat(), ch.getAudioPid(), ch.getAudioFormat(), ch.getModulation(), onPlaying);
    }

    public Define.ResultEnum test_ChannelPlayDown() {

        // Media 중지
        freeMediaPlayer();

        // 채널리스트 얻어옴
        ChannelList chlist = null;
        try {
            chlist = getChannelList();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (chlist.size() < 2) {
            Log.e(TAG, "There has only one channels");
            return Define.ResultEnum.ERROR;
        }
        if ((ChannelPlayIndex - 1) >= 0) {
            ChannelPlayIndex--;
        } else {
            ChannelPlayIndex = 0;
        }

        // 재생할 채널정보 얻어옴
        Channel ch = chlist.get(ChannelPlayIndex);
        Log.i(TAG, "play " + ch.getServiceName());

        // 채널 재생
        return mTvInterface.dtvPlay(ch.getFreq(), ch.getVideoPid(), ch.getVideoFormat(), ch.getAudioPid(), ch.getAudioFormat(), ch.getModulation(), onPlaying);
    }

    public Define.ResultEnum test_LogcatChannels() {
        ChannelList chlist = null;
        try {
            chlist = getChannelList();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < chlist.size(); ++i) {
            Channel ch = chlist.get(i);
            Log.i(TAG, "(" + ch.getMajor() + "-" + ch.getMinor() + ")" + ch.getServiceName());
        }

        return Define.ResultEnum.OK;
    }

    public Define.ResultEnum test_VideoStop() {
        return mTvInterface.dtvStop();
    }

    public Define.ResultEnum test_ScanChannel() {
        if (mScanObserver != null) {
            // Already on scanning
            mTvInterface.stopScan();
        }
        mScanObserver = new ScanObserver();
        Define.ResultEnum b = mTvInterface.startScan(mScanObserver);
        if (b != OK) {
            mScanObserver = null;
        }
        return b;
    }

    public Define.ResultEnum test_ScanChannelStop() {
        if (mScanObserver == null)
            return Define.ResultEnum.BAD_REQUEST;
        mTvInterface.stopScan();
        return OK;
    }

    public Define.ResultEnum test_callSettings() {
        PackageManager packageManager = mContext.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage("com.android.settings");
        if (null != intent) {
            mContext.startActivity(intent);
        }
        return OK;
    }

    public Define.ResultEnum test_callApps() {
        PackageManager packageManager = mContext.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage("net.quber.unoplus.apps");
        if (null != intent) {
            mContext.startActivity(intent);
        }
        return OK;
    }

    static final String AUTHORITY = "net.quber.quantum.DTVContentProvider";
    static final Uri EPG_URI = Uri.parse("content://"+AUTHORITY+"/program_event");
    static final String projection[] = {
            "title", "start_time", "end_time", "duration",
            "event_type", "synopsis", "rating_text"
    };

    public Define.ResultEnum test_currentAllProgram() {
        // 채널리스트 얻어옴
        ChannelList chlist = null;
        try {
            chlist = getChannelList();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Channel ch = chlist.get(ChannelPlayIndex);
        ContentResolver cr = mContext.getContentResolver();
        String selection = " (frequency = " + (ch.getFreq()*1000) + ") AND ( service_id = " + ch.getSid() + " )";
        String[] selArgs = null;
        String sortOrder = " ORDER BY start_time DESC";
        Cursor cur = cr.query(EPG_URI, projection, selection, selArgs, sortOrder);
        if (cur != null && cur.moveToFirst()) {
            for (int i = 0; cur.isAfterLast() == false; cur.moveToNext(), ++i) {
                Log.i(TAG, "title=" + cur.getString(0) + "\nstartTime=" + cur.getInt(1)
                        + "\nendTime="+cur.getInt(2)
                        + "\nduration="+cur.getInt(3)
                        + "\neventType="+cur.getInt(4)
                        + "\nsynopsis="+cur.getString(5)
                        + "\nrating_text="+cur.getString(6));
            }
        }

        return OK;
    }

    public Define.ResultEnum test_currentNowProgram() {
        // 채널리스트 얻어옴
        ChannelList chlist = null;
        try {
            chlist = getChannelList();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Channel ch = chlist.get(ChannelPlayIndex);
        ContentResolver cr = mContext.getContentResolver();
        long findTime = System.currentTimeMillis() / 1000;
        String selection = " (((frequency = " + (ch.getFreq()*1000) + ") AND ( service_id = " + ch.getSid() + " ) AND ( start_time < " + Long.toString(findTime) + " AND end_time >=" + Long.toString(findTime) + " )))";
        String[] selArgs = null;
        String sortOrder = " ORDER BY start_time ASC LIMIT " + 1;
        Cursor cur = cr.query(EPG_URI, projection, selection, selArgs, sortOrder);
        if (cur != null && cur.moveToFirst()) {
            for (int i = 0; cur.isAfterLast() == false; cur.moveToNext(), ++i) {
                Log.i(TAG, "title=" + cur.getString(0) + "\nstartTime=" + cur.getInt(1)
                        + "\nendTime="+cur.getInt(2)
                        + "\nduration="+cur.getInt(3)
                        + "\nsynopsis="+cur.getString(5));
            }
        }
        return OK;
    }
}
