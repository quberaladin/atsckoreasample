package net.quber.atsckoreasample;

import android.app.Activity;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public class MainActivity extends Activity {

    public static final String TAG = "ATSC";
    private ListView mMethodList;
    private ArrayList<String> mMethodNames = new ArrayList<String>();
    private ArrayList<Method> mMethodArray = new ArrayList<Method>();
    private Method[] mMethods;
    public SurfaceView mDisplayView;
    private QuberTestCentral mTestCentral;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mMethodList = (ListView) findViewById(R.id.listView);
        mDisplayView = (SurfaceView) findViewById(R.id.tvview);

        mDisplayView.getHolder().setFormat(PixelFormat.TRANSLUCENT);

        try {
            mTestCentral = QuberTestCentral.getInstance(this);
            mMethods = mTestCentral.getClass().getDeclaredMethods();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Get test method names faild!");
            return;
        }

        for (int i = 0; i < mMethods.length; ++i) {
            if ( Modifier.isPublic(mMethods[i].getModifiers()) && mMethods[i].getName().startsWith("test_")) {
                mMethodNames.add(mMethods[i].getName().substring(5));
                mMethodArray.add(mMethods[i]);
            }
        }

        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_my_list_1, mMethodNames);
        mMethodList.setAdapter(adapter);

        mMethodList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    Log.i(TAG, "Invoke " + mMethodNames.get(i) + ", " + mMethodArray.get(i));
                    mMethodArray.get(i).invoke(mTestCentral, null);
                } catch ( IllegalAccessException e ) {
                    e.printStackTrace();
                } catch ( InvocationTargetException e ) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mMethodList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i(TAG, "onSelected " + i + "-" + l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mMethodList.requestFocus();
    }
}
